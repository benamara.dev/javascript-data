
/*
    - types de données: string, numbers, boolean, tableau, objets
    - afficher des donnés sans classes HTML (multiple classes, multiple id) dans une fonction
*/


//tableau
let tab = [
    "ville1",
    "ville2",
    24,
    true,
    {
        index1 : "valeur1",
        index2 : 2,
    }
];
//console.log(tab[0][4]);

//objet
let objet = {
    index1 : "valeur 1",
    index2 : "valeur 2",
    index3 : ["valeur 31", "valeur32", "valeur 33"],
}
//console.log(objet.index3[0][1]);
objet.id4 = "valeur 4";
//console.log(objet);


// structures de controle
// if else
let data = [
    {
        pseudo : "nom1",
        age : 34,
        techno : ["symfony", "shopify", "scrum"],
        admin : true,
    },
    {
        pseudo : "nom2",
        age : 32,
        techno : ["php", "js", "react", "vuejs"],
        admin : false,
    },
    {
        pseudo : "nom3",
        age : 31,
        techno : ["mangodb", "mysql", "nodejs"],
        admin : false,
    }
];

if (data[1].age > data[2].age) {
  //  console.log(data[1].pseudo + " est plus agée que " +data[2].pseudo);
}else{
  //  console.log("ko");
}

//while
let w = 0;
while (w < 10) {
    w++;
   // console.log("la valeur de w est de : " + w);
}

//les boucle for
for ( i = 0; i < data.length; i++) {

   // console.log(i);
   // console.log(data[i].pseudo);
    document.body.innerHTML += "<h2>"+data[i].pseudo +"</h2>";
}


//if
document.addEventListener("click", function (e) {
//console.log(e.target.id);
    if (e.target.id === "js") {
        document.body.style.background = "yellow";
    }else if (e.target.id === "sf") {
        document.body.style.background = "red";
    } else if (e.target.id === "njs") {
        document.body.style.background = "blue";
    }
})

/**/
//methodes tableau
let arrayNumber = [44,75,55,62,18,11];

// FILTER
console.log(arrayNumber.filter(
    (number) => number > 19//number est une variable 
));

// SORT
console.log(arrayNumber.sort(
    (a, b) => a - b// a et b sont deux variable
));

// FILTER+SORT
console.log(arrayNumber.filter(
    (number) => number > 19
).sort(
    (a, b) => b - a
));

// MAP
document.body.innerHTML = arrayNumber.map(
    (number)=>( "<li>" +number+ "</li>" 
))
                                    .join("")
                                    ;
 

//methodes objet
/* */
document.body.innerHTML = data
.filter(
    (user)=>user.pseudo.includes("n") //pseudo contenant la lettre n
)
.sort((a,b)=> a.age - b.age)
.map(
    (user)=>
            `
                <div class="user__card">
                    <h2>${user.pseudo}</h2>
                    <p>${user.age}</p>
                    <p>${user.admin ? "Modérateur" : "Membre" }</p>
                </div>
            `
)
.join("")

//Dates
//method isoString


let date = new Date();

function newDateFormat(dateFr) {
    let newDate = new Date(dateFr).toLocaleDateString("fr-FR",{
        year: "numeric",
        month: "long",
        day: "numeric"
    });
    return newDate;
}
//console.log(newDateFormat(date));


//Destructuring

let destVar = ["element 1", "element 2", "element 3"]
let [x,y,z] = destVar;
//console.log(z);

//datasets
const h3Py = document.getElementById("py");
//console.log(h3Py.dataset.lang);

//Regex: .search(), .replace(), .match(), 
let mail = "test@test.com"
//console.log(mail.replace(/test/,"de"));








